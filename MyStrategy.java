/*
  Стратегия, версия ＭＫＺ－６「Ｇａｍｍａ」.

  +Добавть проверку башни при походе за бнусом и проверху раша линии(если раш >= 75% - не ходить за бонусом)


  Реализовано:
  Продвинутая система боя с выбором приоритетной и ближайшей цели.
  Примитивные уклонения.
  Продвинутое векторное движение по точкам, с динамической сменой линии.
  Улучшенный обход Юнитов.
  Продвинутое Самосхоронение.
  Улучшенные Модификаторы.
  Разбивка карты на тактические зоны.
  Продвинутый мониторинг бонуса.
  Продвинутый сбор бонуса.
  */


import model.*;
import java.util.*;

enum Role {
    TANK,     //Tank Build
    DAMAGE_DEALER,       //Rusher Build
    DD_SUPPORT, //Support Build
    TANK_SUPPORT,  //Support Build
    BALANCED, //Middle Build
    NULL
}

//tactic
enum LanePart {
    DEFEND,
    BATTLE,
    RUSH,
    SELFBASE,
    ENEMYBASE,
    FOREST,
    RIVER
}
class TacticStatus{
    final LaneType laneType;
    final LanePart lanePart;

    TacticStatus(LaneType lane, LanePart part){
        laneType = lane;
        lanePart = part;
    }
}

class Priority<F, S> {
    private final F first;
    private final S second;
    //конструктор
    Priority(F first, S second){
        super();
        this.first = first;
        this.second = second;
    }

    F getFirst() {
        return first;
    }
    S getSecond() {
        return second;
    }

}

public final class MyStrategy implements Strategy {
    private static final double WAYPOINT_RADIUS = 100.0D;

    //Модификаторы
    private double retainingWizardDistance, recognitionDistance, outflanckDistance, retainingMinionDistance;
    private LaneType defaultLane;
    private boolean gankFlag;

    //модификаторы
    private enum Hitpoits {HIGH, MEDIUM, LOW, CRITICAL, DANGEROUS}
    private Hitpoits hp;


    //Передвижения
    private double sightAngle;//угол отнисительно взгляда
    private Point2D point;

    //Уклонение
    private int evadeInt = 0;
    private int evadeIntAnotherSide = 0;
    private boolean evadeBool;

    //Состояние игрока
    private enum Status {
        FARMING,
        MOVING,
        BONUS,
        GANK}
    private Status status;

    //Tactic
    private LanePart part;

    //War
    private Role role = Role.NULL;
    private boolean frostBolt;
    private boolean fireBall;
    private boolean advancedMagicMissle;
    private boolean shield;
    private boolean haste;
    private double ballCD;
    private double boltCD;

    //Bonuses
    //Проверка появления
    private boolean topBonus = false, botBonus = false;
    //Дополнительный флаг для MIDDLE линии
    private boolean mid;
    //Проверка смерти
    private int tickIndex = 0;


    //Пресекатель багов.
    //Бонус
    private Point2D bugBonPoint;
    private int tickCounter = 0;

    /**
     * Ключевые точки для каждой линии, позволяющие упростить управление перемещением волшебника.
     */
    private final Map<LaneType, Point2D[]> waypointsByLane = new EnumMap<>(LaneType.class);

    private Random random;

    private LaneType lane;
    private Point2D[] waypoints;

    private Wizard self;
    private World world;
    private Game game;
    private Move move;

    /**
     * Основной метод стратегии, осуществляющий управление волшебником.
     * Вызывается каждый тик для каждого волшебника.
     *
     * @param self  Волшебник, которым данный метод будет осуществлять управление.
     * @param world Текущее состояние мира.
     * @param game  Различные игровые константы.
     * @param move  Результатом работы метода является изменение полей данного объекта.
     */
    @Override
    public void move(Wizard self, World world, Game game, Move move) {
        //System.out.print(world.getTickIndex());
        initializeStrategy(self, game);
        initializeTick(self, world, game, move);
        TacticStatus selfTacticStatus = initializeTactic(lane, part, self);
        setModifications();
        setRole(lane);
        bonusCheck(selfTacticStatus);
        bugSuppresser();

        System.out.println(status);

        if (deathCheck()){
            //changeLine(LaneType.BOTTOM);
            status = Status.MOVING;
        }

        if(status == Status.FARMING || status == Status.GANK) {
            evading();
        }

        //обход юнитов
        destroyTree();
        if (getNearestUnit()!=0){
            if (getNearestUnit() == 1 || getNearestUnit() == -1 ) {
                outflankUnitStrict(getNearestUnit());
                return;
            }
            outflankUnitNotStrict(getNearestUnit());
        }


        if (status == Status.BONUS){
            attackTactic(selfTacticStatus);
            if (goToBonus(selfTacticStatus)){
                return;
            }
        }

        if (abandonPoint(selfTacticStatus)) return;

        if (game.isSkillsEnabled()){
            levelUp();
            if (attackTactic(selfTacticStatus)) return;
        }
        else if(attackTarget()) return;

        status = Status.MOVING;
        mid = false;
        goTo(getNextWaypoint());
    }



//*****************Bonus Block******************************//
    /**
     * @param bot - координаты бонуса на ВОТ.
     * @param top - координаты бонуса на ТОР.
     * Бонус считается подобранным, если точка бонуса пуста.
     */
    private void checkBySelf(Point2D bot, Point2D top, TacticStatus selfTacticStatus) {

        //Не работает, если количество тиков до появления бонусов больше вычисленного коэффициента
        //Или меньше, чем константное значение == 200.
        if (world.getTickIndex() % 2500 > 2500 - wayToBonusCompute(selfTacticStatus)) return;
        if (world.getTickIndex() % 2500 < 200) return;

        //если одна из точек с бонусом в зоне видимости
        if (bot.getDistanceTo(self) < self.getVisionRange() || top.getDistanceTo(self) < self.getVisionRange()){
            //Создаем список бонусов, видимых нашей команде
            List<Bonus> bonuses = new ArrayList<>();
            bonuses.addAll(Arrays.asList(world.getBonuses()));
            //Удаляем бонус, вне нашего поля зрения(если он есть)
            bonuses.removeIf(bonus -> bonus.getDistanceTo(self) > self.getVisionRange());
            //Если список пуст(рядом с точкой нет бонуса)
            if (bonuses.isEmpty()){
                //Если волшебник у BОТ линии
                if (bot.getDistanceTo(self) < self.getVisionRange())botBonus = true;
                //Если волшебник у TOP линии
                if (top.getDistanceTo(self) < self.getVisionRange())topBonus = true;
            }
        }
    }

    /**
     * @return true, если волшебник погиб.
     */
    private boolean deathCheck(){
        if (world.getTickIndex() > 0){
            if  (world.getTickIndex() - tickIndex > 1000)
            {
                tickIndex = world.getTickIndex();
                return true;
            }

            tickIndex = world.getTickIndex();
        }
        return false;
    }

    /**
     * @return true, если волшебник в опасной ситуации.
     */
    private boolean dangerCheck(){
        boolean wiz = false;
        boolean min = false;
        boolean hhp = false;

        List<Wizard> wizards = new LinkedList<>();
        wizards.addAll(Arrays.asList(world.getWizards()));
        wizards.removeIf(w -> w.getDistanceTo(self) > w.getCastRange() + 10);
        wizards.removeIf(w -> w.getFaction() == self.getFaction());
        if (wizards.isEmpty() || wizards.size() == 0) {
            wiz = true;
        }

        List<Minion> minions = new LinkedList<>();
        minions.addAll(Arrays.asList(world.getMinions()));
        minions.removeIf(w -> w.getDistanceTo(self) > game.getFetishBlowdartAttackRange() + 10);
        minions.removeIf(w -> w.getFaction() == self.getFaction());
        if (minions.isEmpty() || minions.size() == 0) {
            min = true;
        }

        if (hp == Hitpoits.HIGH || hp == Hitpoits.MEDIUM || hp == Hitpoits.LOW) hhp = true;

        return !(hhp || wiz || min);
    }

    /**
     * @param selfTacticStatus
     * @return - true, если стоит отступать нестандартным методом.
     */
    private boolean dangerCheck(TacticStatus selfTacticStatus){

        List<LivingUnit> enemies = new ArrayList<>();
        enemies.addAll(Arrays.asList(world.getWizards()));
        enemies.addAll(Arrays.asList(world.getMinions()));
        enemies.removeIf(e -> e.getFaction() == self.getFaction() || e.getFaction() == Faction.NEUTRAL);

        if (selfTacticStatus.laneType == LaneType.TOP) {
            enemies.removeIf(e -> e.getX() > 1200 && e.getY() > 1200);
            for (LivingUnit enemy : enemies) {
                if ((selfTacticStatus.lanePart == LanePart.RIVER || selfTacticStatus.lanePart == LanePart.FOREST ||
                selfTacticStatus.lanePart == LanePart.DEFEND)) {
                    if (enemy.getX() < enemy.getY())
                    {
                        //System.out.println(enemy.getX()+"   "+enemy.getY());
                        return true;
                    }
                }
            }
        }
        if (selfTacticStatus.laneType == LaneType.BOTTOM){
            enemies.removeIf(e -> e.getX() < 2800 && e.getY() < 2800);
            for (LivingUnit enemy : enemies){
                if (selfTacticStatus.lanePart == LanePart.RIVER || selfTacticStatus.lanePart == LanePart.FOREST ||
                        selfTacticStatus.lanePart == LanePart.DEFEND){
                    if (enemy.getY() < enemy.getX()){
                        //System.out.println(enemy.getX()+"   "+enemy.getY());
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param selfTacticStatus - Тактический статус
     * Устанавливает значения переменных {@code topBonus} и {@code botBonus} на true через каждый интервал.
     */
    private void bonusCheck(TacticStatus selfTacticStatus){
        List<Building> towers = new ArrayList<>();
        towers.addAll(Arrays.asList(world.getBuildings()));
        //Удаляю все башни принадлежащие совей фракции, вражеские башни, у которых хп больше 15% и башни, расстояние до которых слишком велико.
        towers.removeIf(t -> t.getFaction() == self.getFaction());
        towers.removeIf(t -> t.getDistanceTo(self) > self.getVisionRange() * 1.5);
        towers.removeIf(t -> t.getLife()/t.getMaxLife() > 0.15D);
        if (!towers.isEmpty()) return;

        if (selfTacticStatus.lanePart == LanePart.RUSH){
            if (selfTacticStatus.laneType == LaneType.TOP && self.getX() > 2000) return;
            if (selfTacticStatus.laneType == LaneType.BOTTOM && self.getY() < 2000) return;
            if (selfTacticStatus.laneType == LaneType.MIDDLE && self.getX()>2000 && self.getY() < 2000) return;
        }

        if  (world.getTickIndex() > 1 && world.getTickIndex() % 2500 >= 2500 - wayToBonusCompute(selfTacticStatus)+10 &&
                world.getTickIndex() < 19000){
            topBonus = false;
            botBonus = false;
            if (selfTacticStatus.lanePart == LanePart.BATTLE || selfTacticStatus.lanePart == LanePart.RUSH){
                status = Status.BONUS;
            }
        }
    }

    /**
     * @param selfTacticStatus - такстический статус волшебника.
     * @return true, если волшебник идет за бонусом.
     */
    private boolean goToBonus(TacticStatus selfTacticStatus){
        Point2D topPoint = new Point2D(1200, 1200);
        Point2D botPoint = new Point2D(2800, 2800);

        checkBySelf(botPoint, topPoint, selfTacticStatus);

        outflankUnitStrict(getNearestUnit());
        outflankUnitNotStrict(getNearestUnit());

        if (selfTacticStatus.lanePart == LanePart.RUSH) {
            goTo(getPreviousWaypoint());
            return true;
        }

        if (selfTacticStatus.lanePart == LanePart.RIVER || selfTacticStatus.lanePart == LanePart.BATTLE || selfTacticStatus.lanePart == LanePart.FOREST) {

            if (dangerCheck()) return false;

            if (selfTacticStatus.laneType == LaneType.TOP) {
                Point2D retPoint = new Point2D(400, 400);

                if (topPoint.getDistanceTo(self) > WAYPOINT_RADIUS / 50 && !topBonus) goTo(topPoint);
                else topBonus = true;

                if (mid && topBonus && self.getDistanceTo(2000, 2000) > WAYPOINT_RADIUS * 4) {
                    goTo(new Point2D(2000, 2000));
                    return true;
                }

                if (topBonus) goTo(retPoint);

                if (topBonus && retPoint.getDistanceTo(self) <= WAYPOINT_RADIUS * 4) status = Status.MOVING;
                return true;
            }

            if (selfTacticStatus.laneType == LaneType.BOTTOM) {
                Point2D retPoint = new Point2D(3600, 3600);

                if (botPoint.getDistanceTo(self) > WAYPOINT_RADIUS / 50D && !botBonus) goTo(botPoint);
                else botBonus = true;

                if (botBonus && mid && self.getDistanceTo(2000, 2000) > WAYPOINT_RADIUS * 4D) {
                    goTo(new Point2D(2000, 2000));
                    return true;
                }

                if (botBonus) goTo(retPoint);
                if (botBonus && retPoint.getDistanceTo(self) <= WAYPOINT_RADIUS * 4) status = Status.MOVING;
                return true;
            }

            if (selfTacticStatus.laneType == LaneType.MIDDLE){

                if ((topBonus || botBonus) && self.getDistanceTo(2000, 2000) > WAYPOINT_RADIUS * 3D){
                    goTo(new Point2D(2000, 2000));
                    return true;
                }

                if ((topBonus || botBonus) && self.getDistanceTo(2000, 2000) <= WAYPOINT_RADIUS * 3D){
                    status = Status.MOVING;
                    return false;
                }

                if (botPoint.getDistanceTo(self) < topPoint.getDistanceTo(self) && !botBonus) {
                    mid = true;
                    goTo(botPoint);
                } else
                if (!topBonus){
                    mid = true;
                    goTo(topPoint);
                }
                else return false;

                return true;
            }
        }
        return false;
    }

    /**
     * @param selfTacticStatus Тактический статус.
     */
    private int wayToBonusCompute(TacticStatus selfTacticStatus){
        Point2D adjPoint = null;
        Point2D bot = new Point2D(game.getMapSize() * 0.7, game.getMapSize() * 0.7);
        Point2D top = new Point2D(game.getMapSize() * 0.3, game.getMapSize() * 0.3);
        Point2D point;

        if (selfTacticStatus.lanePart == LanePart.BATTLE){
            if (selfTacticStatus.laneType == LaneType.TOP) adjPoint = new Point2D(game.getMapSize() * 0.15, game.getMapSize() * 0.15);
            if (selfTacticStatus.laneType == LaneType.MIDDLE) adjPoint = new Point2D(game.getMapSize() * 0.5, game.getMapSize() * 0.5);
            if (selfTacticStatus.laneType == LaneType.BOTTOM) adjPoint = new Point2D(game.getMapSize() * 0.85, game.getMapSize() * 0.85);
        }
        if (selfTacticStatus.lanePart == LanePart.RUSH || selfTacticStatus.lanePart == LanePart.DEFEND){
            if (selfTacticStatus.laneType == LaneType.TOP) adjPoint = new Point2D(game.getMapSize() * 0.1, game.getMapSize() * 0.1);
            if (selfTacticStatus.laneType == LaneType.MIDDLE) adjPoint = new Point2D(game.getMapSize() * 0.5, game.getMapSize() * 0.5);
            if (selfTacticStatus.laneType == LaneType.BOTTOM) adjPoint = new Point2D(game.getMapSize() * 0.9, game.getMapSize() * 0.9);
        }
        if (adjPoint == null) {
            return 150;
        }

        if (top.getDistanceTo(self) < bot.getDistanceTo(self)){
            point = top;
        } else point = bot;

        //угол волшебник-бонус
        double angle = Math.abs(self.getAngleTo(adjPoint.getX(), adjPoint.getY()));

        double px = self.getX() + ((adjPoint.getDistanceTo(self) - WAYPOINT_RADIUS * 2) * Math.cos(angle));
        double py = self.getY() + ((adjPoint.getDistanceTo(self) - WAYPOINT_RADIUS * 2) * Math.sin(angle));
        double length = (adjPoint.getDistanceTo(self) - WAYPOINT_RADIUS * 2) + (point.getDistanceTo(px, py));

        return (int) (length/4.5);
    }

    private void bugSuppresserBonus(){
        if (status == Status.BONUS){
            //Если точка не определена или волшебник за её пределами
            if (bugBonPoint == null || bugBonPoint.getDistanceTo(self) > WAYPOINT_RADIUS) {
                //Создать точку в координатах волшебника
                bugBonPoint = new Point2D(self.getX(), self.getY());
                //Обнулить счетчик.
                tickCounter = 0;
            }
            //Если в пределах точки
            if (bugBonPoint.getDistanceTo(self) <= WAYPOINT_RADIUS) {
                //итерировать счетчик каждый тик
                tickCounter++;
                //Если счетчик превысил значение - поменять статус.
                if (tickCounter >= 300) status = Status.MOVING;
            }
        }
    }



//*******************Moving Block************************************//

    /**
     * Данный метод предполагает, что все ключевые точки на линии упорядочены по уменьшению дистанции до последней
     * ключевой точки. Перебирая их по порядку, находим первую попавшуюся точку, которая находится ближе к последней
     * точке на линии, чем волшебник. Это и будет следующей ключевой точкой.
     * <p>
     * Дополнительно проверяем, не находится ли волшебник достаточно близко к какой-либо из ключевых точек. Если это
     * так, то мы сразу возвращаем следующую ключевую точку.
     */
    private Point2D getNextWaypoint() {
        int lastWaypointIndex = waypoints.length - 1;
        Point2D lastWaypoint = waypoints[lastWaypointIndex];

        for (int waypointIndex = 0; waypointIndex < lastWaypointIndex; ++waypointIndex) {
            Point2D waypoint = waypoints[waypointIndex];

            if (waypoint.getDistanceTo(self) <= WAYPOINT_RADIUS) {
                point = waypoints[waypointIndex + 1];
                return waypoints[waypointIndex + 1];
            }

            if (lastWaypoint.getDistanceTo(waypoint) < lastWaypoint.getDistanceTo(self)) {
                point = waypoint;
                return waypoint;
            }
        }
        point = lastWaypoint;
        return lastWaypoint;
    }

    /**
     * Действие данного метода абсолютно идентично действию метода {@code getNextWaypoint}, если перевернуть массив
     * {@code waypoints}.
     * */
    private Point2D getPreviousWaypoint() {
        Point2D firstWaypoint = waypoints[0];

        for (int waypointIndex = waypoints.length - 1; waypointIndex > 0; --waypointIndex) {
            Point2D waypoint = waypoints[waypointIndex];

            if (waypoint.getDistanceTo(self) <= WAYPOINT_RADIUS) {
                point = waypoints[waypointIndex - 1];
                return waypoints[waypointIndex - 1];
            }

            if (firstWaypoint.getDistanceTo(waypoint) < firstWaypoint.getDistanceTo(self)) {
                point = waypoint;
                return waypoint;
            }
        }
        point = firstWaypoint;
        return firstWaypoint;
    }

    /**
     * @param points - массив точек на текущей линии.
     * @return ближайшую точку на текущей линии.
     */
    private Point2D getNearestWaypoint(Point2D[] points){
        double distance = Double.MAX_VALUE;
        Point2D arrivalpoint = null;
        for (int i = 0; i < points.length; i++){
            if (points[i].getDistanceTo(self) < distance) {
                distance = points[i].getDistanceTo(self);
                arrivalpoint = points[i];
            }
        }
        return arrivalpoint;
    }

    /**
     * Перемещение волшебника, используя векторы и направление
     * */
    private void goTo(Point2D point) {

        //угол поворота
        double angle = self.getAngleTo(point.getX(), point.getY());
        //если статус == передвижение
        if(status == Status.MOVING){
            //повернуться
            move.setTurn(angle);
        }
        if (status == Status.BONUS){
            List<LivingUnit> units = getEnemiesInVisionRange(0.8F);
            if (units.isEmpty()){
                move.setTurn(angle);
            }
        }

        double maxFBSpeed = game.getWizardForwardSpeed();
        //если угол поворота больше 180 => инверсия движения
        if (StrictMath.abs(angle) > Math.PI / 2) {
            maxFBSpeed = game.getWizardBackwardSpeed();
        }
        //a*cos(φ) - oY
        //b*sin(φ) - oX
        move.setSpeed(Math.cos(angle) * maxFBSpeed);
        move.setStrafeSpeed(Math.sin(angle) * game.getWizardStrafeSpeed());
    }

    /**
     * Волшебник покидает точку, если
     * 1 - силы сторон неравны
     * 2 - модификаторы Distance
     * 3 - модификаторы ХР
     * */
    private boolean abandonPoint(TacticStatus selfTacticStatus){
        //Количество очков жизней у юнитов, находящихся рядом с волшебником
        double sumAllyFactionScore = 0; //дружественные
        double sumEnemyFactionScore = 0; //враждебные

        outflankUnitStrict(getNearestUnit());
        if (game.isSkillsEnabled()) attackTactic(selfTacticStatus);
        else attackTarget();


        if (hp == Hitpoits.DANGEROUS) {
            goTo(getPreviousWaypoint());
            return true;
        }


        if (crazyLimiterBuildings(getEnemiesInVisionRange(2), selfTacticStatus)){
            //System.out.println("Abandon!(CRAZY)");
            goTo(getPreviousWaypoint());
            return true;
        }



        double distanceToWizard = Double.MAX_VALUE;
        List<LivingUnit> wizards = getEnemiesInVisionRange(1);
        wizards.removeIf(w -> w.getClass() != Wizard.class);
        for (LivingUnit wizard : wizards) if (self.getDistanceTo(wizard) < distanceToWizard) distanceToWizard = self.getDistanceTo(wizard);

        double distanceToMinion = Double.MAX_VALUE;
        List<LivingUnit> minions = getEnemiesInVisionRange(1);
        minions.removeIf(m -> m.getClass() != Minion.class);
        for (LivingUnit minion : minions) if (self.getDistanceTo(minion) < distanceToMinion) distanceToMinion = self.getDistanceTo(minion);

        if (distanceToWizard < retainingWizardDistance) {
            //System.out.println("Abandon!(SO_CLOSE_WIZARD)");
            goTo(getPreviousWaypoint());
            return true;
        }
        if (distanceToMinion < retainingMinionDistance){
            //System.out.println("Abandon!(SO_CLOSE_MINION)");
            goTo(getPreviousWaypoint());
            return true;
        }

        return false;
    }

    /**
     * Возвращает -1, если ближайший юнит слева
     * Возвращает 1, если ближайший юнит справа
     * Возвращает 0, если юнит слишком далеко
     * */
    private int getNearestUnit() {

        if (world.getTickIndex() < 50) return 0;

        CircularUnit nearestUnit = null;
        double nearestUnitDistance = Double.MAX_VALUE;

        //список со всеми юнитами
        List<CircularUnit> units = new LinkedList<>();
        units.addAll(Arrays.asList(world.getBuildings()));
        units.addAll(Arrays.asList(world.getWizards()));
        units.addAll(Arrays.asList(world.getMinions()));
        units.addAll(Arrays.asList(world.getTrees()));

        units.removeIf(unit -> unit.getDistanceTo(self) > self.getVisionRange() / 3);
        units.removeIf(unit -> unit.getId() == self.getId());

        //Для юнита из списка
        for (CircularUnit unit : units) {

            double distance = self.getDistanceTo(unit);
            //если юнит ближе всех остальных - сделать приоритетным
            if (distance < nearestUnitDistance) {
                nearestUnit = unit;
                nearestUnitDistance = distance;
            }
        }

        if (nearestUnit == null || point == null) return 0;

        //Если юнит существует в пределах
        if(nearestUnitDistance <= self.getRadius() + nearestUnit.getRadius() + outflanckDistance){
            //определить угол местоположения относительно движения
            double moveAngle = self.getAngleTo(point.getX(), point.getY()) - self.getAngleTo(nearestUnit);
            //определить угол относительно взгляда(положительное - справа)
            sightAngle = self.getAngleTo(nearestUnit);

            // Если угол < 0 - то Юнит справа относительно движения
            if(moveAngle < 0)return 1;
            else return -1;
        }

        if(nearestUnitDistance <= self.getRadius() + nearestUnit.getRadius() + outflanckDistance * 3){
            //определить угол местоположения относительно движения
            double moveAngle = self.getAngleTo(point.getX(), point.getY()) - self.getAngleTo(nearestUnit);
            //определить угол относительно взгляда(положительное - справа)
            sightAngle = self.getAngleTo(nearestUnit);

            // Если угол < 0 - то Юнит справа относительно движения
            if(moveAngle < 0)return 2;
            else return -2;
        }
        return 0;
    }

    /**
     * Строгий обход волшебником препятствий
     * */
    private void outflankUnitStrict(int foo){

        if(foo == 1 && sightAngle > 0) strafing(true, 1);

        if(foo == 1 && sightAngle < 0) strafing(false, 1);

        if(foo == -1 && sightAngle > 0) strafing(true, 1);

        if(foo == -1 && sightAngle < 0) strafing(false, 1);

    }

    /**
     * Не строгий обход волшебником препятствий
     * */
    private void outflankUnitNotStrict(int foo){

        if(foo == 2 && sightAngle > 0) strafing(true, 1.5);

        if(foo == 2 && sightAngle < 0) strafing(false, 1.5);

        if(foo == -2 && sightAngle > 0) strafing(true, 1.5);

        if(foo == -2 && sightAngle < 0) strafing(false, 1.5);

    }

    private void strafing(boolean b, double exp){
        move.setStrafeSpeed(b ? -game.getWizardStrafeSpeed() / exp : game.getWizardStrafeSpeed() / exp);
    }

    /**
     * Уничтожает ближайшее дерево.
     */
    private void destroyTree(){
        Tree[] trees = world.getTrees();

        for(Tree tree : trees){

            if(self.getDistanceTo(tree) < self.getRadius() + tree.getRadius() + 10.0D){

                double distance = self.getDistanceTo(tree);
                double angle = self.getAngleTo(tree);

                move.setTurn(angle);
                if (StrictMath.abs(angle) < game.getStaffSector() / 2.0D) {
                    move.setAction(ActionType.STAFF);
                    move.setCastAngle(angle);
                    move.setMinCastDistance(distance - tree.getRadius());
                }
            }
        }
    }

    /**
     * Инциализируем стратегию.
     * <p>
     * Для этих целей обычно можно использовать конструктор, однако в данном случае мы хотим инициализировать генератор
     * случайных чисел значением, полученным от симулятора игры.
     */
    private void initializeStrategy(Wizard self, Game game) {
        if (random == null) {
            random = new Random(game.getRandomSeed());

            double mapSize = game.getMapSize();

            waypointsByLane.put(LaneType.MIDDLE, new Point2D[]{
                    new Point2D(100.0D, mapSize - 100.0D),
                    random.nextBoolean() ? new Point2D(600.0D, mapSize - 200.0D) :
                            new Point2D(200.0D, mapSize - 600.0D),
                    new Point2D(800.0D, mapSize - 800.0D),
                    new Point2D(1200.0D, mapSize - 1200.0D),
                    new Point2D(1600.0D, mapSize - 1600.0D),
                    new Point2D(mapSize/2, mapSize/2),
                    new Point2D(mapSize - 1600.0D, 1600.0D),
                    new Point2D(mapSize - 1200.0D,1200.0D),
                    new Point2D(mapSize - 800.0D,800.0D),
                    new Point2D(mapSize - 600.0D, 600.0D)

            });

            waypointsByLane.put(LaneType.TOP, new Point2D[]{
                    new Point2D(100.0D, mapSize - 100.0D),
                    new Point2D(100.0D, mapSize - 400.0D),
                    new Point2D(100.0D, mapSize - 800.0D),
                    new Point2D(150.0D, mapSize * 0.75D),//обходим первую башню
                    new Point2D(200.0D, mapSize *  0.7D),//обходим первую башню
                    new Point2D(150.0D, mapSize * 0.65D),//обходим первую башню
                    new Point2D(100.0D, mapSize *  0.6D),
                    new Point2D(100.0D, mapSize * 0.55D),
                    new Point2D(100.0D, mapSize *  0.5D),
                    new Point2D(100.0D, mapSize * 0.45D),// Вторая башня
                    new Point2D(150.0D, mapSize *  0.4D),// Вторая башня
                    new Point2D(200.0D, mapSize * 0.35D),
                    new Point2D(250.0D, mapSize *  0.3D),
                    new Point2D(250.0D, mapSize * 0.25D),
                    new Point2D(250.0D, mapSize *  0.2D),
                    new Point2D(mapSize * 0.125D, mapSize * 0.125D),  //Граничная точка между ТОР и RIVER
                    new Point2D(mapSize * 0.2D,  250),
                    new Point2D(mapSize * 0.25D, 200),
                    new Point2D(mapSize * 0.3D,  200),
                    new Point2D(mapSize * 0.35D, 200),//первая башня соперника
                    new Point2D(mapSize * 0.4D,  200),//первая башня соперника
                    new Point2D(mapSize * 0.45D, 200),
                    new Point2D(mapSize * 0.5D,  200),
                    new Point2D(mapSize * 0.55D, 200),
                    new Point2D(mapSize * 0.6D,  200),
                    new Point2D(mapSize * 0.65D, 200), //вторая башня соперника
                    new Point2D(mapSize * 0.7D,  300),
                    new Point2D(mapSize * 0.75D, 300),
                    new Point2D(mapSize * 0.8D,  300),
                    new Point2D(mapSize * 0.85D, 300), //фонтан противника
                    new Point2D(mapSize * 0.9D,  200), //фонтан противника
            });

            waypointsByLane.put(LaneType.BOTTOM, new Point2D[]{
                    new Point2D(100.0D, mapSize - 100.0D),
                    new Point2D(mapSize * 0.15D, mapSize - 100.0D),
                    new Point2D(mapSize * 0.2D,  mapSize - 200.0D),
                    new Point2D(mapSize * 0.25D, mapSize - 200.0D),
                    new Point2D(mapSize * 0.3D,  mapSize - 200.0D),
                    new Point2D(mapSize * 0.35D, mapSize - 200.0D), //Первая башня
                    new Point2D(mapSize * 0.4D,  mapSize - 200.0D),
                    new Point2D(mapSize * 0.45D, mapSize - 200.0D),
                    new Point2D(mapSize * 0.5D,  mapSize - 200.0D),
                    new Point2D(mapSize * 0.55D, mapSize - 200.0D),
                    new Point2D(mapSize * 0.6D,  mapSize - 200.0D), //Вторая башня
                    new Point2D(mapSize * 0.65D, mapSize - 200.0D), //Вторая башня
                    new Point2D(mapSize * 0.7D,  mapSize - 200.0D),
                    new Point2D(mapSize * 0.75D, mapSize - 300.0D),
                    new Point2D(mapSize * 0.8D,  mapSize - 300.0D),
                    new Point2D(mapSize * 0.85D, mapSize - 600.0D), //Граница между BOTTOM и RIVER
                    new Point2D(mapSize - 300.0D,  mapSize * 0.8D),
                    new Point2D(mapSize - 300.0D, mapSize * 0.75D),
                    new Point2D(mapSize - 300.0D,  mapSize * 0.7D),
                    new Point2D(mapSize - 200.0D, mapSize * 0.65D), //первая башня противника
                    new Point2D(mapSize - 200.0D,  mapSize * 0.6D), //первая башня противника
                    new Point2D(mapSize - 200.0D, mapSize * 0.55D), //первая башня противника
                    new Point2D(mapSize - 300.0D,  mapSize * 0.5D),
                    new Point2D(mapSize - 300.0D, mapSize * 0.45D),
                    new Point2D(mapSize - 300.0D,  mapSize * 0.4D),
                    new Point2D(mapSize - 300.0D, mapSize * 0.35D),//вторая башня противника
                    new Point2D(mapSize - 300.0D,  mapSize * 0.3D),//вторая башня противника
                    new Point2D(mapSize - 300.0D, mapSize * 0.25D),
                    new Point2D(mapSize - 300.0D,  mapSize * 0.2D),
                    new Point2D(mapSize - 300.0D, mapSize * 0.15D),
                    new Point2D(mapSize - 300.0D,  mapSize * 0.1D),  //Фонтан
                    new Point2D(mapSize - 300.0D, mapSize * 0.05D), //Фонтан

            });

            switch ((int) self.getId()) {
                case 1:
                case 2:
                case 6:
                case 7:
                    lane = LaneType.TOP; //TOP
                    break;
                case 3:
                case 8:
                    lane = LaneType.MIDDLE; //MID
                    break;
                case 4:
                case 5:
                case 9:
                case 10:
                    lane = LaneType.BOTTOM; //BOT
                    break;
                default:
            }
            waypoints = waypointsByLane.get(lane);
            defaultLane = lane;
        }
    }

    /**
     * @param lane - линия, на которую надо поменяться.
     * Смена линии.
     */
    private void changeLine(LaneType lane){
        if (lane == LaneType.TOP) waypoints = waypointsByLane.get(LaneType.TOP);
        if (lane == LaneType.BOTTOM) waypoints = waypointsByLane.get(LaneType.BOTTOM);
        if (lane == LaneType.MIDDLE) waypoints = waypointsByLane.get(LaneType.MIDDLE);
    }

    /**
     * Вспомогательный класс для хранения позиций на карте.
     */
    private static final class Point2D {
        private final double x;
        private final double y;

        private Point2D(double x, double y) {
            this.x = x;
            this.y = y;
        }

        public double getX() {
            return x;
        }

        public double getY() {
            return y;
        }

        double getDistanceTo(double x, double y) {
            return StrictMath.hypot(this.x - x, this.y - y);
        }

        double getDistanceTo(Point2D point) {
            return getDistanceTo(point.x, point.y);
        }

        double getDistanceTo(Unit unit) {
            return getDistanceTo(unit.getX(), unit.getY());
        }
    }



//***********************War Block****************************************//

    private LivingUnit getNearestTarget(){
        List<LivingUnit> nearestTargetList = getEnemiesInVisionRange(1);
        LivingUnit resultTarget = null;
        double distanceToNearestTarget = Double.MAX_VALUE;

        for (LivingUnit nearestTarget : nearestTargetList){
            if (self.getDistanceTo(nearestTarget) < distanceToNearestTarget) {
                distanceToNearestTarget = self.getDistanceTo(nearestTarget);
                resultTarget = nearestTarget;
            }
        }
        if (resultTarget != null){
            if (resultTarget.getDistanceTo(self) > game.getStaffRange()) return null;
            return resultTarget;
        }
        return null;
    }

    private void createGank() {
        List<LivingUnit> wizards = getEnemiesInVisionRange(1);
        wizards.removeIf(w -> w.getClass() != Wizard.class);

        if (status != Status.BONUS) {
            if (!wizards.isEmpty()) {
                if ((hp == Hitpoits.MEDIUM || hp == Hitpoits.HIGH) && gankFlag) {
                    status = Status.GANK;
                }
            }
        }
    }

    private LivingUnit getGankTarget(){
        LivingUnit gankTarget = null;
        List<LivingUnit> wizards = getEnemiesInVisionRange(1.5F);
        wizards.removeIf(w -> w.getClass() != Wizard.class);

        double distance = Double.MAX_VALUE;
        for (LivingUnit wizard : wizards){
            if (self.getDistanceTo(wizard) < distance){
                distance = self.getDistanceTo(wizard);
                gankTarget = wizard;
            }
        }
        return  gankTarget;
    }

    private boolean attackTacticBalancer(TacticStatus selfTacticStatus){
        createGank();
        outflankUnitStrict(getNearestUnit());
        outflankUnitNotStrict(getNearestUnit());

        LivingUnit priorityTarget = getPriorityTarget(false);
        LivingUnit priorityTargetInCastRange = getPriorityTarget(true);
        LivingUnit gankTarget = getGankTarget();
        LivingUnit nearestTarget = getNearestTarget();

        //Для ближайшей цели - если она в зоне поражения - бить посохом по кулдауну и обстреливать ракетой в упор.
        if (nearestTarget != null){
            status = Status.FARMING;
            double angleToNearestTarget = self.getAngleTo(nearestTarget);
            double distanceToNearestTarget = self.getDistanceTo(nearestTarget);
            move.setTurn(angleToNearestTarget);

            if (StrictMath.abs(angleToNearestTarget) < game.getStaffSector() / 2){
                move.setAction(ActionType.STAFF);
                throwMISSLE(angleToNearestTarget, distanceToNearestTarget, nearestTarget);
                return true;
            }
            return false;
        }

        if (status == Status.GANK) {
            List<LivingUnit> wizards = getEnemiesInVisionRange(1);
            wizards.removeIf(w -> w.getClass() != Wizard.class);
            double distanceToGankTarget = Double.MAX_VALUE;

            for (LivingUnit wizard : wizards) {
                if (wizard.getDistanceTo(self) < distanceToGankTarget) {
                    distanceToGankTarget = wizard.getDistanceTo(self);
                    gankTarget = wizard;
                }
            }

            if (gankTarget == null) return false;
            if (gankTarget.getDistanceTo(self) > self.getCastRange()) goTo(new Point2D(gankTarget.getX(), gankTarget.getY()));
            else {
                double angleToGankTarget = self.getAngleTo(gankTarget);
                move.setTurn(angleToGankTarget);

                if (StrictMath.abs(angleToGankTarget) < game.getStaffSector() / 2.0D) {
                    if (frostBolt) {
                        if (self.getMana() / self.getMaxMana() > 0.1) {
                            if (world.getTickIndex() >= boltCD + game.getFrostBoltCooldownTicks()) {
                                boltCD = throwFROSTBOLT(angleToGankTarget, distanceToGankTarget, gankTarget);
                                return true;
                            }
                        }
                    }
                    if (gankTarget.getDistanceTo(self) <= game.getStaffRange()) {
                        move.setAction(ActionType.STAFF);
                        return true;
                    }
                    throwMISSLE(angleToGankTarget, distanceToGankTarget, gankTarget);
                    return true;
                }
            }
            return false;
        }

        if (priorityTarget != null) {
            double distanceToPriorityTarget = self.getDistanceTo(priorityTarget);

            if (status != Status.BONUS) status = Status.FARMING;

            if (distanceToPriorityTarget <= self.getCastRange()) {
                double angleToPriorityTarget = self.getAngleTo(priorityTarget);
                move.setTurn(angleToPriorityTarget);

                if (StrictMath.abs(angleToPriorityTarget) < game.getStaffSector() / 2) {

                    if (frostBolt && priorityTarget.getClass() == Wizard.class) {

                        if (world.getTickIndex() >= boltCD + game.getFrostBoltCooldownTicks()) {
                            boltCD = throwFROSTBOLT(angleToPriorityTarget, distanceToPriorityTarget, priorityTarget);
                            return true;
                        }
                    }
                    throwMISSLE(angleToPriorityTarget, distanceToPriorityTarget, priorityTarget);
                    return true;
                }
                return false;
            } else {
                //Если в близи нету целей
                if (priorityTargetInCastRange == null) return false;
                double angle = self.getAngleTo(priorityTargetInCastRange);
                double distanceInCastRange = self.getDistanceTo(priorityTargetInCastRange);

                move.setTurn(angle);

                //если нацелился
                if (StrictMath.abs(angle) < game.getStaffSector() / 2.0D) {
                    if (priorityTargetInCastRange.getDistanceTo(self) <= game.getStaffRange()){
                        move.setAction(ActionType.STAFF);
                        return true;
                    }
                    throwMISSLE(angle, distanceInCastRange, priorityTargetInCastRange);
                    return true;
                }
            }
        }

        return false;
    }

    private boolean attackTacticDamageDealer(){

        createGank();

        LivingUnit priorityTarget = getPriorityTarget(false);
        LivingUnit priorityTargetInCastRange = getPriorityTarget(true);
        LivingUnit gankTarget = null;

        //обход юнитов во время боя
        outflankUnitStrict(getNearestUnit());

        if (status == Status.GANK) {
            List<LivingUnit> wizards = getEnemiesInVisionRange(1.5F);
            wizards.removeIf(w -> w.getClass() != Wizard.class);
            double distance = Double.MAX_VALUE;
            for (LivingUnit wizard : wizards) {
                if (wizard.getDistanceTo(self) < distance) {
                    distance = wizard.getDistanceTo(self);
                    gankTarget = wizard;
                }
            }
            if (gankTarget == null) return false;
            if (gankTarget.getDistanceTo(self) > self.getCastRange()) goTo(new Point2D(gankTarget.getX(), gankTarget.getY()));
            double angle = self.getAngleTo(gankTarget);
            move.setTurn(angle);
            if (StrictMath.abs(angle) < game.getStaffSector() / 2.0D) {
                if (fireBall) {
                    if (self.getMana() / self.getMaxMana() > 0.1) {
                        if (world.getTickIndex() >= ballCD + game.getFireballCooldownTicks()) {
                            ballCD = throwFIREBALL(angle, distance, gankTarget);
                            return true;
                        }
                    }
                    if (gankTarget.getDistanceTo(self) <= self.getRadius() + gankTarget.getRadius() + game.getStaffRange()) {
                        move.setAction(ActionType.STAFF);
                        return true;
                    }
                }
                throwMISSLE(angle, distance, gankTarget);
                return true;
            }
        }

        // Если видим противника ...
        if (priorityTarget != null) {
            double distance = self.getDistanceTo(priorityTarget);
            if (status != Status.BONUS) status = Status.FARMING;

            //В радиусе атаки
            if (distance <= self.getCastRange()) {
                double angle = self.getAngleTo(priorityTarget);
                //Если противник - здание
                if (priorityTarget.getClass() == Building.class) {
                    //если изучен шар
                    if (fireBall) {
                        //если близок к башне - добивать посохом
                        if (self.getDistanceTo(priorityTarget) <= 201) {
                            //если хп больше половины
                            if (hp == Hitpoits.HIGH) {
                                retainingWizardDistance = 0;
                                goTo(new Point2D(priorityTarget.getX(), priorityTarget.getY()));
                                //Начать бить, если подошел вплотную
                                if (self.getDistanceTo(priorityTarget) <= self.getRadius() + priorityTarget.getRadius() + game.getStaffRange()) {
                                    move.setAction(ActionType.STAFF);
                                }
                            }
                        }
                        if (self.getMana() / self.getMaxMana() >= 0.3) {
                            if (world.getTickIndex() >= ballCD + game.getFireballCooldownTicks()) {
                                //Не зависимо от того, где волшебник относительно башни - накладывать бафф ГОРЕНИЕ.
                                move.setTurn(angle);

                                //если нацелился
                                if (StrictMath.abs(angle) < game.getStaffSector() / 2.0D) {
                                    model.Status[] statuses = priorityTarget.getStatuses();
                                    if (statuses.length == 0) {
                                        ballCD = throwFIREBALL(angle, distance, priorityTarget);
                                        return true;
                                    }
                                    for (int i = 0; i < statuses.length; i++) {
                                        if (statuses[i].getType() != StatusType.BURNING) {
                                            ballCD = throwFIREBALL(angle, distance, priorityTarget);
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        throwMISSLE(angle, distance, priorityTarget);
                        return true;
                    }

                }

                if (priorityTarget.getClass() == Wizard.class) {
                    if (fireBall) {
                        if (self.getMana() / self.getMaxMana() > 0.3) {
                            ballCD = throwFIREBALL(angle, distance, priorityTarget);
                            return true;

                        }
                        if (priorityTarget.getDistanceTo(self) <= self.getRadius() + priorityTarget.getRadius() + game.getStaffRange()){
                            move.setAction(ActionType.STAFF);
                            return true;
                        }
                    }
                    throwMISSLE(angle, distance, priorityTarget);
                    return true;
                }
                if (priorityTarget.getClass() == Minion.class) {
                    if (fireBall) {
                        if (world.getTickIndex() >= ballCD + game.getFireballCooldownTicks()) {
                            List<LivingUnit> enemies = new ArrayList<>();
                            enemies.addAll(Arrays.asList(world.getWizards()));
                            enemies.addAll(Arrays.asList(world.getMinions()));
                            enemies.addAll(Arrays.asList(world.getBuildings()));
                            enemies.removeIf(e -> e.getDistanceTo(priorityTarget) > game.getFireballExplosionMinDamageRange());
                            if (enemies.size() > 2) {
                                ballCD = throwFIREBALL(angle, distance, priorityTarget);
                                return true;
                            }
                        }
                        if (priorityTarget.getDistanceTo(self) <= self.getRadius() + priorityTarget.getRadius() + game.getStaffRange()){
                            move.setAction(ActionType.STAFF);
                            return true;
                        }
                    }
                }
            } else {
                //Если в близи нету целей
                if (priorityTargetInCastRange == null) return false;
                double angle = self.getAngleTo(priorityTargetInCastRange);
                double distanceInCastRange = self.getDistanceTo(priorityTargetInCastRange);

                move.setTurn(angle);

                //если нацелился
                if (StrictMath.abs(angle) < game.getStaffSector() / 2.0D) {
                    if (priorityTargetInCastRange.getDistanceTo(self) <= self.getRadius() + priorityTargetInCastRange.getRadius() +
                            game.getStaffRange()){
                        move.setAction(ActionType.STAFF);
                        return true;
                    }
                    throwMISSLE(angle, distanceInCastRange, priorityTargetInCastRange);
                    return true;
                }
            }
        }
        return false;
    }

    private boolean attackTactic(TacticStatus selfTacticStatus){
        if (role == Role.BALANCED) return attackTacticBalancer(selfTacticStatus);
        //it will be never happen
        return false;
    }

    private void levelUp(){
        if (role == Role.BALANCED) levelUpBalancedRole();
    }

    private void setRole(LaneType lane){
        role = Role.BALANCED;
    }

    private void levelUpDamageDealer(){
        //Advanced Missle Root
        if (self.getLevel() == 1) move.setSkillToLearn(SkillType.RANGE_BONUS_PASSIVE_1);
        if (self.getLevel() == 2) move.setSkillToLearn(SkillType.RANGE_BONUS_AURA_1);
        if (self.getLevel() == 3) move.setSkillToLearn(SkillType.RANGE_BONUS_PASSIVE_2);
        if (self.getLevel() == 4) move.setSkillToLearn(SkillType.RANGE_BONUS_AURA_2);
        if (self.getLevel() == 5){
            move.setSkillToLearn(SkillType.ADVANCED_MAGIC_MISSILE);
            advancedMagicMissle = true;
        }
        //Fireball Root
        if (self.getLevel() == 6) move.setSkillToLearn(SkillType.STAFF_DAMAGE_BONUS_PASSIVE_1);
        if (self.getLevel() == 7) move.setSkillToLearn(SkillType.STAFF_DAMAGE_BONUS_AURA_1);
        if (self.getLevel() == 8) move.setSkillToLearn(SkillType.STAFF_DAMAGE_BONUS_PASSIVE_2);
        if (self.getLevel() == 9) move.setSkillToLearn(SkillType.STAFF_DAMAGE_BONUS_AURA_2);
        if (self.getLevel() == 10){
            move.setSkillToLearn(SkillType.FIREBALL);
            fireBall = true;
        }
        //Shield Root
        if (self.getLevel() == 11) move.setSkillToLearn(SkillType.MAGICAL_DAMAGE_ABSORPTION_PASSIVE_1);
        if (self.getLevel() == 12) move.setSkillToLearn(SkillType.MAGICAL_DAMAGE_ABSORPTION_AURA_1);
        if (self.getLevel() == 13) move.setSkillToLearn(SkillType.MAGICAL_DAMAGE_ABSORPTION_PASSIVE_2);
        if (self.getLevel() == 14) move.setSkillToLearn(SkillType.MAGICAL_DAMAGE_ABSORPTION_AURA_2);


    }

    private void levelUpBalancedRole(){
        //Advanced Missle Root
        if (self.getLevel() == 1) move.setSkillToLearn(SkillType.RANGE_BONUS_PASSIVE_1);
        if (self.getLevel() == 2) move.setSkillToLearn(SkillType.RANGE_BONUS_AURA_1);
        if (self.getLevel() == 3) move.setSkillToLearn(SkillType.RANGE_BONUS_PASSIVE_2);
        if (self.getLevel() == 4) move.setSkillToLearn(SkillType.RANGE_BONUS_AURA_2);
        if (self.getLevel() == 5){
            move.setSkillToLearn(SkillType.ADVANCED_MAGIC_MISSILE);
            advancedMagicMissle = true;
        }
        //FrostBolt Root
        if (self.getLevel() == 6) move.setSkillToLearn(SkillType.MAGICAL_DAMAGE_BONUS_PASSIVE_1);
        if (self.getLevel() == 7) move.setSkillToLearn(SkillType.MAGICAL_DAMAGE_BONUS_AURA_1);
        if (self.getLevel() == 8) move.setSkillToLearn(SkillType.MAGICAL_DAMAGE_BONUS_PASSIVE_2);
        if (self.getLevel() == 9) move.setSkillToLearn(SkillType.MAGICAL_DAMAGE_BONUS_AURA_2);
        if (self.getLevel() == 10){
            move.setSkillToLearn(SkillType.FROST_BOLT);
            frostBolt = true;
        }
        //Shield Root
        if (self.getLevel() == 11) move.setSkillToLearn(SkillType.MAGICAL_DAMAGE_ABSORPTION_PASSIVE_1);
        if (self.getLevel() == 13) move.setSkillToLearn(SkillType.MAGICAL_DAMAGE_ABSORPTION_AURA_1);

        //Haste Root
        if (self.getLevel() == 12) move.setSkillToLearn(SkillType.MOVEMENT_BONUS_FACTOR_PASSIVE_1);
        if (self.getLevel() == 14) move.setSkillToLearn(SkillType.MOVEMENT_BONUS_FACTOR_AURA_1);


    }

    private double throwFIREBALL(double angle, double distance, LivingUnit target){
        move.setAction(ActionType.FIREBALL);
        move.setCastAngle(angle);
        move.setMinCastDistance(distance - target.getRadius() + game.getFireballRadius());
        return world.getTickIndex();
    }

    private double throwFROSTBOLT(double angle, double distance, LivingUnit target){
        move.setAction(ActionType.FROST_BOLT);
        move.setCastAngle(angle);
        move.setMinCastDistance(distance - target.getRadius() + game.getFrostBoltRadius());
        return world.getTickIndex();
    }

    private void throwMISSLE(double angle, double distance, LivingUnit target){
        move.setAction(ActionType.MAGIC_MISSILE);
        move.setCastAngle(angle);
        move.setMinCastDistance(distance - target.getRadius() + game.getMagicMissileRadius());
    }

    /**
     * Выбирает самую приоритетную цель из списка.
     * @param flag - дополнительная проверка на растояние.
     * @return её.
     */
    private LivingUnit getPriorityTarget(boolean flag){
        //список строений
        List<LivingUnit> buildings = new ArrayList<>();
        buildings.addAll(Arrays.asList(world.getBuildings()));
        //список игроков
        List<LivingUnit> wizards = new ArrayList<>();
        wizards.addAll(Arrays.asList(world.getWizards()));
        //список крипов
        List<LivingUnit> minions = new ArrayList<>();
        minions.addAll(Arrays.asList(world.getMinions()));

        if  (flag){
            buildings.removeIf(b -> b.getDistanceTo(self) > self.getCastRange());
            wizards.removeIf(w -> w.getDistanceTo(self) > self.getCastRange());
            minions.removeIf(m -> m.getDistanceTo(self) > self.getCastRange());
        }

        //из каждого типа берем по самому приоритетному
        Priority<LivingUnit, Double> building = getPriorityUnit(buildings, 90);
        Priority<LivingUnit, Double> wizard = getPriorityUnit(wizards, 90);
        Priority<LivingUnit, Double> minion = getPriorityUnit(minions, 40);

        //выборка приоритетов среди приоритетных
        if(building.getSecond() > wizard.getSecond()){
            if(building.getSecond() > minion.getSecond()){
                return building.getFirst();
            } else {
                return minion.getFirst();
            }
        } else {
            if(wizard.getSecond() > minion.getSecond()){
                return wizard.getFirst();
            } else {
                return minion.getFirst();
            }
        }
    }

    /**
     * Расчитывает приоритетную цель из списка целей.
     * @param targets список целей.
     * @param priority начальный приоритет.
     * @return пару (цель/приоритет)
     */
    private Priority<LivingUnit, Double> getPriorityUnit(List<LivingUnit> targets, double priority){
        Double mPriority = 0.0;
        LivingUnit unit = null;

        //для каждого в списке
        for (LivingUnit target : targets) {
            //если не из своей фракции
            if (target.getFaction() == self.getFaction()) {
                continue;
            }
            double distance = self.getDistanceTo(target);
            //если дистанция больше чем модификатор обзора волшебника
            if(distance > recognitionDistance){
                continue;
            }
            double curPriority = getPriority(target, priority);
            //если не из фракции НЕЙТРАЛ
            if(target.getFaction() == Faction.NEUTRAL){
                continue;
            }
            //если текущий приоритет больше максимального
            if (curPriority > mPriority) {
                unit = target;
                mPriority = priority;

            }
        }
        return new Priority<>(unit, mPriority);
    }

    /**
     * @param unit цель.
     * @param _priority её приоритет.
     * @return значение приоритета после модификации.
     * Модифицирует приоритет по количеству жизней.
     */
    private double getPriority(LivingUnit unit, double _priority){
        double priority = _priority;
        priority += ((unit.getMaxLife() - (double)unit.getLife()) / (double)unit.getMaxLife()) * _priority;
        return priority;
    }

    /**
     * Система боя с выбором приоритетной цели и ближайшей.
     */
    private boolean attackTarget(){
        createGank();

        LivingUnit priorityTarget = getPriorityTarget(false);
        LivingUnit priorityTargetInCastRange = getPriorityTarget(true);
        LivingUnit gankTarget = null;

        //обход юнитов во время боя
        outflankUnitStrict(getNearestUnit());

        if (status == Status.GANK) {
            List<LivingUnit> wizards = getEnemiesInVisionRange(1.5F);
            wizards.removeIf(w -> w.getClass() != Wizard.class);
            double distance = Double.MAX_VALUE;
            for (LivingUnit wizard : wizards) {
                if (wizard.getDistanceTo(self) < distance) {
                    distance = wizard.getDistanceTo(self);
                    gankTarget = wizard;
                }
            }
            if (gankTarget == null) return false;
            if (gankTarget.getDistanceTo(self) > self.getCastRange()) goTo(new Point2D(gankTarget.getX(), gankTarget.getY()));
            double angle = self.getAngleTo(gankTarget);
            move.setTurn(angle);
            if (StrictMath.abs(angle) < game.getStaffSector() / 2.0D) {
                throwMISSLE(angle, distance, gankTarget);
                return true;
            }
        }

        // Если видим противника ...
        if (priorityTarget != null) {
            double distance = self.getDistanceTo(priorityTarget);
            status = Status.FARMING;

            // ... и он в пределах досягаемости наших заклинаний, ...
            if (distance <= self.getCastRange()) {
                double angle = self.getAngleTo(priorityTarget);

                // ... то поворачиваемся к цели.
                move.setTurn(angle);

                // Если цель перед нами, ...
                if (StrictMath.abs(angle) < game.getStaffSector() / 2.0D) {
                    // ... то атакуем.
                    move.setAction(ActionType.MAGIC_MISSILE);
                    move.setCastAngle(angle);
                    move.setMinCastDistance(distance - priorityTarget.getRadius() + game.getMagicMissileRadius());
                }
                return true;
            } else {
                if (priorityTargetInCastRange == null) return false;
                double angle = self.getAngleTo(priorityTargetInCastRange);
                double distanceInCastRange = self.getDistanceTo(priorityTargetInCastRange);

                move.setTurn(angle);

                if (StrictMath.abs(angle) < game.getStaffSector() / 2.0D) {
                    // ... то атакуем.
                    move.setAction(ActionType.MAGIC_MISSILE);
                    move.setCastAngle(angle);
                    move.setMinCastDistance(distanceInCastRange - priorityTargetInCastRange.getRadius() + game.getMagicMissileRadius());
                }
            }
        }
        return false;
    }

    /**
     * Примитивное "уклонение"
     * */
    private void evading(){
        outflankUnitStrict(getNearestUnit());
        outflankUnitNotStrict(getNearestUnit());

        //до тех пор, пока уклонение сторону != 0
        if (evadeInt != 0) {
            //двигаемся
            move.setStrafeSpeed(evadeBool ? game.getWizardStrafeSpeed() : -game.getWizardStrafeSpeed());
            evadeInt -= 1;
        }else if(evadeIntAnotherSide != 0){
            move.setStrafeSpeed(!evadeBool ? game.getWizardStrafeSpeed() : -game.getWizardStrafeSpeed());
            evadeIntAnotherSide -= 1;
        }

        //если значения уклонений не установлено...
        if (evadeIntAnotherSide == 0) {
            //то устанавливаем их
            evadeInt = random.nextInt(15)+5;
            evadeBool = random.nextBoolean();
            evadeIntAnotherSide = evadeInt;
        }

    }

    private boolean crazyLimiterBuildings(List <LivingUnit> enemyList, TacticStatus selfTacticStatus) {
        for (LivingUnit unit : enemyList) {

            if (unit.getClass() == Building.class) {

                if (selfTacticStatus.lanePart == LanePart.ENEMYBASE &&
                        world.getTickIndex() % game.getFactionMinionAppearanceIntervalTicks() < 100){
                    return true;
                }

                if (world.getTickIndex() < 1000) {
                    if (self.getDistanceTo(unit) <= game.getGuardianTowerAttackRange()) return true;
                }

                if (status != Status.BONUS) {
                    int val = 0;
                    for (LivingUnit ally : getAlliesInVisionRange(1)) {
                        if (ally.getDistanceTo(unit) < self.getDistanceTo(unit) && ally.getLife() > game.getGuardianTowerDamage()) {
                            val++;
                        }
                    }
                    if (val < 1) return true;
                }
            }
        }
        return false;
    }



//********************Mod Block**********************************************//

    /**
     * Устанавливает модификаторы.
     */
    private void setModifications(){
        getModifications();
        setModificationsOnHitpoints();
        setModificationsOnStatus();
    }

    /**
     * Обновляет состояния волшебника.
     */
    private void getModifications(){
        if (self.getLife() >= self.getMaxLife() * 0.9D) hp = Hitpoits.HIGH;
        if (self.getLife() < self.getMaxLife() * 0.9D && self.getLife() >= self.getMaxLife() * 0.75D) hp = Hitpoits.MEDIUM;
        if (self.getLife() < self.getMaxLife() * 0.75D && self.getLife() >= self.getMaxLife() * 0.5D) hp = Hitpoits.LOW;
        if (self.getLife() < self.getMaxLife() * 0.5D && self.getLife() >= self.getMaxLife() * 0.25D) hp = Hitpoits.CRITICAL;
        if (self.getLife() < self.getMaxLife() * 0.25D) hp = Hitpoits.DANGEROUS;
    }

    /**
     * Устанавливает значения модификаторов для боя в зависимости от количества здоровья.
     */
    private void setModificationsOnHitpoints(){

        if (hp == Hitpoits.HIGH) {
            recognitionDistance = self.getVisionRange() * 1.5; // Дистанция, на которой распознаются цели
            retainingWizardDistance = 200; // Дистанция, на которой держится волшебник от других волшебников
            retainingMinionDistance = 200;
            gankFlag = true;
        }

        if (hp == Hitpoits.MEDIUM) {
            recognitionDistance = self.getVisionRange();
            retainingWizardDistance = game.getWizardCastRange() - 100;
            retainingMinionDistance = game.getFetishBlowdartAttackRange() - 100;
        }

        if (hp == Hitpoits.LOW){
            recognitionDistance = self.getCastRange() + 100;
            retainingWizardDistance = game.getWizardCastRange() - 50;
            retainingMinionDistance = game.getFetishBlowdartAttackRange() - 50;
            gankFlag = false;
        }

        if (hp == Hitpoits.CRITICAL){
            recognitionDistance = self.getCastRange() - 50;
            retainingWizardDistance = game.getWizardCastRange() + 50;
            retainingMinionDistance = game.getFetishBlowdartAttackRange() + 50;
            createShield();
        }
    }

    /**
     * Устанавливает значения модификаторов для обхода в зависимости от статуса.
     */
    private void setModificationsOnStatus(){
        if (status == Status.FARMING) outflanckDistance = 2;
        if (status == Status.MOVING || status == Status.BONUS) outflanckDistance = 6;
    }

    /**
     * Проверяем местоположение волшебника по общей карте и устанавливаем тактический модификатор.
     * */
    private TacticStatus initializeTactic(LaneType type, LanePart part, Unit unit){
        double x = unit.getX();
        double y = unit.getY();
        double m = game.getMapSize();

        if (y >= m * 0.8 && x <= m * 0.2){
            part = LanePart.SELFBASE;
            type = LaneType.MIDDLE;
        }
        if (x >= m * 0.7 && y < m * 0.3){
            part = LanePart.ENEMYBASE;
            type = LaneType.MIDDLE;
        }
        if (x <=m * 0.2 && y <= m * 0.2){
            type = LaneType.TOP;
            part = LanePart.BATTLE;
        }
        if ((x >= m * 0.4 && x <= m * 0.6) && (y >= m * 0.4 && y <= m * 0.6)){
            type = LaneType.MIDDLE;
            part = LanePart.BATTLE;
        }
        if (x >= m * 0.8 && y >= m * 0.8){
            type = LaneType.BOTTOM;
            part = LanePart.BATTLE;
        }
        if (y < m * 0.9 && part != LanePart.SELFBASE && part != LanePart.BATTLE){
            type = LaneType.BOTTOM;
            part = LanePart.DEFEND;
        }
        if (x < m * 0.1 && part != LanePart.SELFBASE && part != LanePart.BATTLE){
            type = LaneType.TOP;
            part = LanePart.DEFEND;
        }
        if (x < m * 1.1 - y && x > m * 0.9 - y && x < m * 0.5 && y > m * 0.5 && part != LanePart.BATTLE){
            type = LaneType.MIDDLE;
            part = LanePart.DEFEND;
        }
        if (x < m * 1.1 + y && x > -m * 0.9 + y && x > m * 0.5 && y < m * 0.5 && part != LanePart.BATTLE){
            type = LaneType.MIDDLE;
            part = LanePart.RUSH;
        }
        if (x > m * 0.9 && part != LanePart.BATTLE && part != LanePart.ENEMYBASE){
            type = LaneType.BOTTOM;
            part = LanePart.RUSH;
        }
        if (y < m * 0.1 && part != LanePart.BATTLE && part != LanePart.ENEMYBASE){
            type = LaneType.TOP;
            part = LanePart.RUSH;
        }

        if (x > y - m * 0.1 && x < y + m * 0.1 && x < m * 0.5 && y < m * 0.5 && part != LanePart.BATTLE){
            type = LaneType.TOP;
            part = LanePart.RIVER;
        }
        if (x > y - m * 0.1 && x < y + m * 0.1 && x > m * 0.5 && y > m * 0.5 && part != LanePart.BATTLE){
            type = LaneType.BOTTOM;
            part = LanePart.RIVER;
        }
        if ( part != LanePart.RIVER && part != LanePart.RUSH && part != LanePart.BATTLE &&
                part != LanePart.DEFEND && part != LanePart.ENEMYBASE && part != LanePart.SELFBASE)
            part = LanePart.FOREST;

        return new TacticStatus(type, part);
    }

    private void createShield(){
        if (shield) move.setAction(ActionType.SHIELD);
    }

//**********************Universal Block***************************************//

    private void bugSuppresser(){
        bugSuppresserBonus();
    }

    private List<LivingUnit> getEnemiesInVisionRange(float exp){
        List<LivingUnit> enemies = new LinkedList<>();
        enemies.addAll(Arrays.asList(world.getMinions()));
        enemies.addAll(Arrays.asList(world.getWizards()));
        enemies.addAll(Arrays.asList(world.getBuildings()));
        enemies.removeIf(unit ->
                (unit.getFaction() == self.getFaction())|
                        (unit.getFaction() == Faction.OTHER)|
                        (unit.getFaction() == Faction.NEUTRAL)
        );
        enemies.removeIf(unit -> self.getDistanceTo(unit) > self.getVisionRange() * exp);
        return enemies;
    }

    private List<LivingUnit> getAlliesInVisionRange(float exp){
        List<LivingUnit> allies = new LinkedList<>();
        allies.addAll(Arrays.asList(world.getMinions()));
        allies.addAll(Arrays.asList(world.getWizards()));
        allies.addAll(Arrays.asList(world.getBuildings()));
        allies.removeIf(unit -> unit.getFaction() != self.getFaction());
        allies.removeIf(unit -> self.getDistanceTo(unit) > self.getVisionRange() * exp);
        return allies;
    }

    /**
     * Сохраняем все входные данные в полях класса для упрощения доступа к ним.
     */
    private void initializeTick(Wizard self, World world, Game game, Move move) {
        this.self = self;
        this.world = world;
        this.game = game;
        this.move = move;
    }
}